\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}GENERALIZATION}{7}% 
\contentsline {section}{\numberline {1.1}System Description}{7}% 
\contentsline {section}{\numberline {1.2}System Analysis \& Requirement Specification}{7}% 
\contentsline {chapter}{\numberline {2}SRS DOCUMENTS}{10}% 
\contentsline {section}{\numberline {2.1}Determine Actor - Use-case}{10}% 
\contentsline {section}{\numberline {2.2}Use-case diagram}{12}% 
\contentsline {section}{\numberline {2.3}Usecase Description}{13}% 
\contentsline {subsection}{\numberline {2.3.1}Login}{13}% 
\contentsline {subsection}{\numberline {2.3.2}Import new books}{14}% 
\contentsline {subsection}{\numberline {2.3.3}Search}{15}% 
\contentsline {subsection}{\numberline {2.3.4}Update Book's information}{16}% 
\contentsline {subsection}{\numberline {2.3.5}Update Quantity of book}{16}% 
\contentsline {subsection}{\numberline {2.3.6}Add book to Cart}{17}% 
\contentsline {subsection}{\numberline {2.3.7}Sell}{18}% 
\contentsline {subsection}{\numberline {2.3.8}Management employee}{19}% 
\contentsline {subsection}{\numberline {2.3.9}Search employee}{20}% 
\contentsline {subsection}{\numberline {2.3.10}Create Employee Account}{20}% 
\contentsline {subsection}{\numberline {2.3.11}Update Employee Account}{21}% 
\contentsline {subsection}{\numberline {2.3.12}Change Permission Account}{21}% 
\contentsline {subsection}{\numberline {2.3.13}View Report}{22}% 
\contentsline {subsection}{\numberline {2.3.14}View Low Quantity}{23}% 
\contentsline {chapter}{REFERENCE MATERIAL}{24}% 
