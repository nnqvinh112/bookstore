\babel@toc {english}{}
\beamer@sectionintoc {1}{System Description}{3}{0}{1}
\beamer@sectionintoc {2}{System Analysis \& Requirement Specification}{5}{0}{2}
\beamer@sectionintoc {3}{SRS Document}{11}{0}{3}
\beamer@subsectionintoc {3}{1}{Determine Actor-Usecase}{12}{0}{3}
\beamer@subsectionintoc {3}{2}{UseCase Diagram}{15}{0}{3}
\beamer@subsectionintoc {3}{3}{Use-Case Description}{16}{0}{3}
