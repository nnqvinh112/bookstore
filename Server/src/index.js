const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const books = require("./services/books.service");
const employees = require("./services/employees.service");
const genres = require("./services/genres.service");
const authentication = require("./services/authentication.service");
const sellInvoice = require("./services/invoice.service");
const importInvoice = require("./services/import.service");
const report = require("./services/report.service");

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "bookstoredb",
  multipleStatements: true
});

connection.connect();

const port = process.env.PORT || 8080;

const app = express()
  .use(cors())
  .use(bodyParser.json())
  .use(authentication.createRouter(connection))
  .use((req, res, next) => authentication.auth(connection, req, res, next))
  .use(books.createRouter(connection))
  .use(sellInvoice.createRouter(connection))
  .use(importInvoice.createRouter(connection))
  .use(genres.createRouter(connection))
  .use(employees.createRouter(connection))
  .use(report.createRouter(connection));

app.listen(port, () => {
  console.log(`Express server listening on port ${port}`);
});
