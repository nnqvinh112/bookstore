const BaseModel = require("./base.model");

class Account extends BaseModel {
  constructor(item) {
    super(item);
    if (item) {
      this.email = item.email;
      this.phone = item.phone;
      this.role = item.role;
    }
  }
}

module.exports = Account;
