class BaseModel {
  constructor(item) {
    if (item) {
      this.id = item.id;
      this.name = item.name;
    }
  }
}

module.exports = BaseModel;
