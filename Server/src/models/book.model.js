const Genre = require("./genre.model");
const BaseModel = require("./base.model");
const Employee = require("./employee.model");

class Book extends BaseModel {
  constructor(item) {
    super(item);
    if (item) {
      this.title = item.title;
      this.author = item.author || "";
      this.publisher = item.publisher || "";
      this.image = item.image;
      this.description = item.description || "";
      this.amount = item.amount || 0;
      this.price = item.price || 0;
      const genre = new Genre({
        id: item.genreId,
        name: item.genreName
      });
      if (item.genre && typeof item.genre === "number") {
        genre.id = item.genre;
      }
      this.genre = genre;

      this.createdDate = item.createdDate;
      this.updatedDate = item.updatedDate;

      const createdBy = new Employee({
        id: item.createdById,
        name: item.createdByName
      });
      if (item.createdBy && typeof item.createdBy === "number") {
        createdBy.id = item.createdBy;
      }
      this.createdBy = createdBy;

      const updatedBy = new Employee({
        id: item.updatedById,
        name: item.updatedByName
      });
      if (item.updatedBy && typeof item.updatedBy === "number") {
        updatedBy.id = item.updatedBy;
      }
      this.updatedBy = updatedBy;
    }
  }

  flatten() {
    return {
      ...this,
      genre: this.genre.id,
      createdBy: this.createdBy.id,
      updatedBy: this.updatedBy.id
    };
  }
}

module.exports = Book;
