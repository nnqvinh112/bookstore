const Account = require("./account.model");
class Employee extends Account {
  constructor(item) {
    super(item);
    this.createdDate = item.createdDate;
  }

  //   flatten() {
  //     return {
  //       ...this,
  //       email: this.email
  //     };
  //   }
}

module.exports = Employee;
