const BaseModel = require("./base.model");

class Genre extends BaseModel {
  constructor(item) {
    super(item);
  }
}

module.exports = Genre;
