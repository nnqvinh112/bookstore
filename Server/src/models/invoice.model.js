const Employee = require("./employee.model");
const Book = require("./book.model");

class Invoice {
  constructor(item) {
    if (item) {
      this.totalAmount = item.totalAmount || 0;
      this.totalValue = item.totalValue || 0;
      this.id =
        item.id ||
        item.invoiceId ||
        item.invoiceID ||
        item.importId ||
        item.importID;
      this.products = item.products || [];

      this.createdDate = item.createdDate;
      const createdBy = new Employee({
        id: item.createdById,
        name: item.createdByName
      });
      if (item.createdBy && typeof item.createdBy === "number") {
        createdBy.id = item.createdBy;
      }
      this.createdBy = createdBy;
    }
  }

  static fromSql(rows) {
    if (rows && rows.length) {
      const invoice = rows[0];
      return new Invoice({
        ...invoice,
        id: invoice.invoiceID || invoice.importID,
        products: (rows || []).map(i => ({
          id: i.productID,
          amount: i.amount,
          price: i.price
        }))
      });
    }
    return null;
  }
}

module.exports = Invoice;
