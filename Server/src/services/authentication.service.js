const Account = require("../models/account.model");
const express = require("express");

function getUserByCredentials(db, req) {
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT id, name, phone, email, role from employee WHERE email = ? AND password = ?`,
      [req.body.email, req.body.password],
      (error, results) => {
        if (error) {
          return reject(error);
        }
        const res = (results || []).map(i => new Account(i));
        resolve(res.length ? res[0] : null);
      }
    );
  });
}

function getUserByEmail(db, params) {
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT id, name, phone, email, role from employee WHERE email = ?`,
      [params.email],
      (error, results) => {
        if (error) {
          return reject(error);
        }
        const res = (results || []).map(i => new Account(i));
        resolve(res.length ? res[0] : null);
      }
    );
  });
}

function createRouter(db) {
  const router = express.Router();

  // the routes are defined here

  router.post("/auth", async (req, res) => {
    try {
      const user = await getUserByCredentials(db, req);
      if (!user) {
        return res.status(404).json({
          status: "error",
          message: "Not found"
        });
      }
      res.status(200).json(user);
    } catch (error) {
      res.status(500).json({
        status: "error",
        message: error
      });
    }
  });

  return router;
}

const auth = async (db, req, res, next) => {
  const header = req.header('Authorization');
  try {
    const user = await getUserByEmail(db, {
      email: header
    })
    if (!user) {
      throw new Error('User not found')
    }
    req.user = user;
    next()
  } catch (error) {
    res.status(401).send({
      error: 'Not authorized to access this resource'
    })
  }
}

module.exports = {
  createRouter,
  getUserByEmail,
  getUserByCredentials,
  auth
};