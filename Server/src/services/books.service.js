const Book = require("../models/book.model");
const { auth } = require("./authentication.service");
const express = require("express");

function getFilterString(query) {
  const params = [];
  let queryString = "";
  if (query) {
    if (query.id) {
      params.push(`book.id LIKE '%${query.id}%'`);
    }
    if (query.title) {
      params.push(`book.title LIKE '%${query.title}%'`);
    }
    if (query.publisher) {
      params.push(`book.publisher LIKE '%${query.publisher}%'`);
    }
    if (query.author) {
      params.push(`book.author LIKE '%${query.author}%'`);
    }
    if (query.genre) {
      params.push(`genre.name LIKE '%${query.genre}%'`);
    }
  }
  if (params.length > 0) {
    queryString = `WHERE ${params.join(" OR ")}`;
  }
  return queryString;
}

function getAll(db, req) {
  let filterString = getFilterString(req.query);
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT book.id, book.title, book.publisher, book.author, book.image, book.amount, book.price, book.description, genre.id as genreId, genre.name as genreName FROM book
      JOIN genre on book.genreID = genre.id
      ${filterString}
      ORDER BY title`,
      [],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve((results || []).map(i => new Book(i)));
      }
    );
  });
}

function getBooksByGenre(db, req) {
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT book.id, book.title, book.publisher, book.author, book.image, book.amount, book.price, book.description, genre.id as genreId, genre.name as genreName FROM book
      JOIN genre on book.genreID = genre.id
      WHERE book.genreID = ?
      ORDER BY title`,
      [req.params.genreId],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve((results || []).map(i => new Book(i)));
      }
    );
  });
}

function getBooksByIds(db, ids) {
  const query = (ids || []).map(i => `id = ${i}`).join(" OR ");
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT id, title, price, amount from book WHERE ${query}`,
      [],
      (error, result) => {
        if (error) {
          return reject(error);
        }
        const books = (result || []).map(i => new Book(i));
        resolve(books);
      }
    );
  });
}

function getBooksWithLowAmount(db, req) {
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT book.id, book.title, book.publisher, book.author, book.image, book.amount, book.price, book.description, genre.id as genreId, genre.name as genreName FROM book
      JOIN genre on book.genreID = genre.id
      WHERE book.amount <= ?
      ORDER BY title`,
      [req.params.amount],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve((results || []).map(i => new Book(i)));
      }
    );
  });
}

function getSingle(db, req) {
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT book.id, book.title, book.publisher, book.author, book.image, book.amount, book.price, book.description, book.createdDate, book.updatedDate,
      employee.id as createdById, employee.name as createdByName,
      genre.id as genreId, genre.name as genreName FROM book
      JOIN genre on book.genreID = genre.id
      JOIN employee on book.createdBy = employee.id
      WHERE book.id = ?
      `,
      [req.params.id],
      (error, results) => {
        if (error) {
          return reject(error);
        }
        const res = (results || []).map(i => new Book(i));
        resolve(res.length ? res[0] : null);
      }
    );
  });
}

async function increaseBookAmounts(db, books) {
  return new Promise((resolve, reject) => {
    const queries = (books || [])
      .map(i => {
        return `UPDATE book SET amount = amount + ${i.amount} WHERE id = ${i.id}`;
      })
      .join(";");
    db.query(queries, [], (error, result) => {
      if (error) {
        return reject(error);
      }
      resolve(result);
    });
  });
}

function postSingle(db, req) {
  const book = new Book(req.body).flatten();
  return new Promise((resolve, reject) => {
    db.query(
      "INSERT INTO book (title, description, genreID, amount, price, createdBy, updatedBy, author, publisher) VALUES (?,?,?,?,?,?,?,?,?)",
      [
        book.title,
        book.description,
        book.genre,
        book.amount,
        book.price,
        req.user.id,
        req.user.id,
        book.author,
        book.publisher
      ],
      error => {
        if (error) {
          return reject(error);
        }
        resolve(true);
      }
    );
  });
}

function updateSingle(db, req) {
  const id = req.params.id;
  const book = new Book(req.body).flatten();
  return new Promise((resolve, reject) => {
    db.query(
      "UPDATE book SET title=?, description=?, genreID=?, amount=?, price=?, updatedBy=?, updatedDate=?, author=?, publisher=? WHERE id = ?",
      [
        book.title,
        book.description,
        book.genre,
        book.amount,
        book.price,
        req.user.id,
        new Date(),
        book.author,
        book.publisher,
        +id
      ],
      error => {
        if (error) {
          return reject(error);
        }
        resolve(true);
      }
    );
  });
}

function createRouter(db) {
  const router = express.Router();

  // the routes are defined here

  router.post("/book", async (req, res, next) => {
    try {
      await postSingle(db, req);
      res.status(200).json({ status: "ok" });
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  router.get("/book", async (req, res, next) => {
    try {
      const books = await getAll(db, req, res);
      
      res.status(200).json(books);
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  router.get("/book/genre/:genreId", async (req, res, next) => {
    try {
      const books = await getBooksByGenre(db, req, res);
      res.status(200).json(books);
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  router.get("/book/shortage/:amount", async (req, res, next) => {
    try {
      const books = await getBooksWithLowAmount(db, req, res);
      res.status(200).json(books);
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  router.get("/book/:id", async (req, res, next) => {
    try {
      const book = await getSingle(db, req);
      res.status(200).json(book);
    } catch (error) {
      res.status(500).json({ status: "error" });
    }
  });

  router.put("/book/:id", async (req, res, next) => {
    try {
      await updateSingle(db, req);
      res.status(200).json({ status: "ok" });
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  return router;
}

module.exports = {
  createRouter,
  getBooksByIds,
  increaseBookAmounts
};
