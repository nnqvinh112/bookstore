const Employee = require("../models/employee.model");
const express = require("express");
function getAll(db, req) {
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT employee.id, employee.name, employee.phone, employee.email, employee.role, employee.createdDate FROM employee
        `,
      // ORDER BY id LIMIT 10
      [10 * (req.params.page || 0)],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve((results || []).map(i => new Employee(i)));
      }
    );
  });
}
function getSingle(db, req) {
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT employee.id, employee.name, employee.phone, employee.email, employee.role, employee.createdDate FROM employee
        WHERE employee.id = ?
        `,
      [req.params.id],
      (error, results) => {
        if (error) {
          return reject(error);
        }
        const res = (results || []).map(i => new Employee(i));
        resolve(res.length ? res[0] : null);
      }
    );
  });
}
function updateSingle(db, req) {
  const id = req.params.id;
  console.log("id", id);
  const employee = new Employee(req.body);
  return new Promise((resolve, reject) => {
    db.query(
      "UPDATE employee SET name=?, phone=?,email=?, role=? WHERE id = ?",
      [employee.name, employee.phone, employee.email, employee.role, +id],
      error => {
        if (error) {
          return reject(error);
        }
        resolve(true);
      }
    );
  });
}
function post(db, req) {
  const employee = new Employee(req.body);

  return new Promise((resolve, reject) => {
    db.query(
      "INSERT INTO employee (name, phone, email, role, createdBy, createdDate) VALUES (?,?,?,?,?,?)",
      [
        employee.name,
        employee.phone,
        employee.email,
        employee.role,
        req.user.id,
        new Date()
      ],
      error => {
        if (error) {
          return reject(error);
        }
        resolve(true);
      }
    );
  });
}

function createRouter(db) {
  const router = express.Router();
  router.get("/employee", async (req, res, next) => {
    try {
      const employees = await getAll(db, req, res);
      res.status(200).json(employees);
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });
  router.post("/employee", async (req, res, next) => {
    try {
      await post(db, req);

      res.status(200).json({ status: "ok" });
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });
  router.get("/employee/:id", async (req, res, next) => {
    try {
      const employee = await getSingle(db, req);
      res.status(200).json(employee);
    } catch (error) {
      res.status(500).json({ status: "error" });
    }
  });
  router.put("/employee/:id", async (req, res, next) => {
    console.log("asasdasd");
    console.log(db);
    try {
      await updateSingle(db, req);
      res.status(200).json({ status: "ok" });
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
    console.log("test");
  });

  return router;
}
module.exports = { createRouter };
