const express = require("express");

function getAll(db, req, res) {
  return new Promise((resolve, reject) => {
    db.query(
      "SELECT id, name FROM genre ORDER BY name",
      [],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(results);
      }
    );
  });
}

function getSingle(db, req, res) {
  return new Promise((resolve, reject) => {
    db.query(
      "SELECT id, name FROM genre WHERE id == ?",
      [req.params.id],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(results);
      }
    );
  });
}

function createRouter(db) {
  const router = express.Router();

  // the routes are defined here
  router.get("/genre", async function(req, res) {
    try {
      const genres = await getAll(db, req, res);
      res.status(200).json(genres);
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  return router;
}

module.exports = {
  createRouter,
  getAll,
  getSingle
};
