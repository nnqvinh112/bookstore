const express = require("express");
const Book = require("../models/book.model");
const Invoice = require("../models/invoice.model");
const { getBooksByIds, increaseBookAmounts } = require("./books.service");
const _ = require("lodash");

function getInvoices(db, req) {
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT importmapper.productID, importmapper.amount, importmapper.price, importmapper.importID, 
        import.totalAmount, import.totalValue, import.createdDate,
        employee.id as createdById, employee.name as createdByName
      FROM importmapper 
      JOIN import ON import.id = importmapper.importID
      JOIN employee ON import.createdBy = employee.id`,
      [],
      (error, result) => {
        if (error) {
          return reject(error);
        }
        const invoices = _.values(_.groupBy(result, "importID"));
        resolve(invoices.map(i => Invoice.fromSql(i)));
      }
    );
  });
}

function getImportById(db, req) {
  const id = req.params.id;
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT importmapper.productID, importmapper.amount, importmapper.price, importmapper.importID, 
        import.totalAmount, import.totalValue, import.createdDate,
        employee.id as createdById, employee.name as createdByName
      FROM importmapper 
      JOIN import ON import.id = importmapper.importID
      JOIN employee ON import.createdBy = employee.id
      WHERE import.id = ?`,
      [id],
      (error, result) => {
        if (error) {
          return reject(error);
        }
        resolve(Invoice.fromSql(result));
      }
    );
  });
}

async function postImportMapper(db, req, importID, books) {
  return new Promise((resolve, reject) => {
    const values = (books || [])
      .map(i => {
        return `(${i.id}, ${importID}, ${i.amount}, ${
          i.price
        }, '${new Date().toISOString()}')`;
      })
      .join(",");
    db.query(
      `INSERT INTO importmapper(productID, importID, amount, price, createdDate) VALUES ${values}`,
      [],
      (error, result) => {
        if (error) {
          return reject(error);
        }
        resolve(importID);
      }
    );
  });
}

async function postImport(db, req) {
  const inputBooks = req.body.books || [];
  const dbBooks = (await getBooksByIds(db, inputBooks.map(i => i.id))).map(
    i => {
      i.amount = inputBooks.find(x => x.id == i.id).amount;
      return i;
    }
  );
  const totalAmount = dbBooks.map(i => i.amount).reduce((a, b) => a + b, 0);
  const totalValue = dbBooks
    .map(i => i.amount * i.price)
    .reduce((a, b) => a + b, 0);
  return new Promise((resolve, reject) => {
    db.query(
      `INSERT INTO import(totalAmount, totalValue, createdBy, createdDate) VALUES (?,?,?,?)`,
      [totalAmount, totalValue, req.user.id, new Date()],
      (error, result) => {
        if (error) {
          return reject(error);
        }
        const importID = result.insertId;
        resolve(importID);
      }
    );
  }).then(id =>
    Promise.all([
      postImportMapper(db, req, id, dbBooks),
      increaseBookAmounts(db, dbBooks)
    ]).then(() => id)
  );
}

function createRouter(db) {
  const router = express.Router();

  // the routes are defined here
  router.get("/import", async function(req, res) {
    try {
      const invoices = await getInvoices(db, req);
      res.status(200).json(invoices);
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  router.get("/import/:id", async function(req, res) {
    try {
      const invoice = await getImportById(db, req);
      res.status(200).json(invoice);
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  router.put("/import/book/:id", async (req, res, next) => {
    try {
      req.body = {
        books: [
          {
            id: req.params.id,
            amount: req.body.amount
          }
        ]
      };
      await postImport(db, req);
      res.status(200).json({ status: "ok" });
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  return router;
}

module.exports = {
  createRouter
};
