const express = require("express");
const Book = require("../models/book.model");
const Invoice = require("../models/invoice.model");
const { getBooksByIds, increaseBookAmounts } = require("./books.service");
const _ = require("lodash");

function checkStock(db, books) {
  const query = (books || [])
    .map(i => {
      return `id = ${i.id} AND amount >= ${i.amount}`;
    })
    .join(" OR ");
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT COUNT(id) as count from book WHERE ${query}`,
      [],
      (error, result) => {
        if (error) {
          return reject(error);
        }
        resolve(result[0].count === books.length);
      }
    );
  });
}

function getInvoices(db, req) {
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT invoicemapper.productID, invoicemapper.amount, invoicemapper.price, invoicemapper.invoiceID, 
        invoice.totalAmount, invoice.totalValue, invoice.createdDate,
        employee.id as createdById, employee.name as createdByName
      FROM invoicemapper 
      JOIN invoice ON invoice.id = invoicemapper.invoiceID
      JOIN employee ON invoice.createdBy = employee.id`,
      [],
      (error, result) => {
        if (error) {
          return reject(error);
        }
        const invoices = _.values(_.groupBy(result, "invoiceID"));
        resolve(invoices.map(i => Invoice.fromSql(i)));
      }
    );
  });
}

function getInvoiceById(db, req) {
  const id = req.params.id;
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT invoicemapper.productID, invoicemapper.amount, invoicemapper.price, invoicemapper.invoiceID, 
        invoice.totalAmount, invoice.totalValue, invoice.createdDate,
        employee.id as createdById, employee.name as createdByName
      FROM invoicemapper 
      JOIN invoice ON invoice.id = invoicemapper.invoiceID
      JOIN employee ON invoice.createdBy = employee.id
      WHERE invoice.id = ?`,
      [id],
      (error, result) => {
        if (error) {
          return reject(error);
        }
        resolve(Invoice.fromSql(result));
      }
    );
  });
}

function reduceBookAmounts(db, req, books) {
  return increaseBookAmounts(
    db,
    books.map(i => {
      i.amount = -i.amount;
      return i;
    })
  );
}

async function postInvoiceMapper(db, req, invoiceId, books) {
  return new Promise((resolve, reject) => {
    const values = (books || [])
      .map(i => {
        return `(${i.id}, ${invoiceId}, ${i.amount}, ${
          i.price
        }, '${new Date().toISOString()}')`;
      })
      .join(",");
    db.query(
      `INSERT INTO invoicemapper(productID, invoiceID, amount, price, createdDate) VALUES ${values}`,
      [],
      (error, result) => {
        if (error) {
          return reject(error);
        }
        resolve(invoiceId);
      }
    );
  });
}

async function postInvoice(db, req) {
  const inputBooks = req.body.books;
  const dbBooks = (await getBooksByIds(
    db,
    (req.body.books || []).map(i => i.id)
  )).map(i => {
    i.amount = inputBooks.find(x => x.id == i.id).amount;
    return i;
  });
  const totalAmount = dbBooks.map(i => i.amount).reduce((a, b) => a + b, 0);
  const totalValue = dbBooks
    .map(i => i.amount * i.price)
    .reduce((a, b) => a + b, 0);
  return new Promise((resolve, reject) => {
    db.query(
      `INSERT INTO invoice(totalAmount, totalValue, createdBy, createdDate) VALUES (?,?,?,?)`,
      [totalAmount, totalValue, req.user.id, new Date()],
      (error, result) => {
        if (error) {
          return reject(error);
        }
        const invoiceId = result.insertId;
        resolve(invoiceId);
      }
    );
  }).then(id =>
    Promise.all([
      postInvoiceMapper(db, req, id, dbBooks),
      reduceBookAmounts(db, req, dbBooks)
    ]).then(() => id)
  );
}

function createRouter(db) {
  const router = express.Router();

  // the routes are defined here
  router.post("/invoice", async function(req, res) {
    try {
      const books = (req.body.books || []).map(i => new Book(i));
      const isStockAvailable = await checkStock(db, books);
      if (!isStockAvailable) {
        return res.status(404).json({
          status: "error",
          message:
            "Stock is not enough or there are books that do not exist in storage"
        });
      }

      const invoiceId = await postInvoice(db, req);
      res.status(200).json({ status: "ok", invoiceId });
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  router.get("/invoice", async function(req, res) {
    try {
      const invoices = await getInvoices(db, req);
      res.status(200).json(invoices);
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  router.get("/invoice/:id", async function(req, res) {
    try {
      const invoice = await getInvoiceById(db, req);
      res.status(200).json(invoice);
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  return router;
}

module.exports = {
  createRouter
};
