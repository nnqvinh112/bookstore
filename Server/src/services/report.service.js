const express = require("express");
const _ = require("lodash");

function reportByInvoice(db, req) {
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT COUNT(id) as totalInvoice, IFNULL(SUM(totalValue), 0) as totalValue
      FROM invoice 
      WHERE createdDate BETWEEN DATE_SUB(NOW(), INTERVAL ? DAY) AND NOW()`,
      [req.query.dayOffset || 30],
      (error, result) => {
        if (error) {
          return reject(error);
        }
        resolve(result.pop());
      }
    );
  });
}

function reportByImport(db, req) {
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT COUNT(id) as totalInvoice, IFNULL(SUM(totalValue), 0) as totalValue
      FROM import 
      WHERE createdDate BETWEEN DATE_SUB(NOW(), INTERVAL ? DAY) AND NOW()`,
      [req.query.dayOffset || 30],
      (error, result) => {
        if (error) {
          return reject(error);
        }
        resolve(result.pop());
      }
    );
  });
}

function reportByGenre(db, req) {
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT book.genreID as id, genre.name, count(genre.id) as totalBook, 
        sum(invoicemapper.amount) as soldAmount, 
        sum(invoicemapper.amount * invoicemapper.price) as soldValue 
        FROM invoicemapper 
      JOIN book on invoicemapper.productID = book.id join genre on genre.id = book.genreID 
      WHERE invoicemapper.createdDate BETWEEN DATE_SUB(NOW(), INTERVAL ? DAY) AND NOW()
      GROUP by genre.id`,
      [req.query.dayOffset || 30],
      (error, result) => {
        if (error) {
          return reject(error);
        }
        resolve(result);
      }
    );
  });
}

function reportByBook(db, req) {
  const genre = req.query.genre || req.query.genreID;
  let queryByGenre = "";
  if (genre) {
    queryByGenre = `AND book.genreID = ${genre}`;
  }
  return new Promise((resolve, reject) => {
    db.query(
      `SELECT book.title, book.id, book.genreID, book.price, book.amount, SUM(invoicemapper.amount) as soldAmount, SUM(invoicemapper.price) as soldValue
      FROM invoicemapper
      JOIN book on book.id = invoicemapper.productID
      WHERE invoicemapper.createdDate BETWEEN DATE_SUB(NOW(), INTERVAL ? DAY) AND NOW() ${queryByGenre}
      GROUP BY book.id
      ORDER BY ${req.query.orderBy || "soldAmount"} DESC
      LIMIT ?`,
      [req.query.dayOffset || 30, +req.query.top || 5],
      (error, result) => {
        if (error) {
          return reject(error);
        }
        resolve(result);
      }
    );
  });
}

function createRouter(db) {
  const router = express.Router();

  // the routes are defined here
  router.get("/report/genre", async function(req, res) {
    try {
      const report = await reportByGenre(db, req);
      res.status(200).json(report);
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  router.get("/report/book", async function(req, res) {
    try {
      const report = await reportByBook(db, req);
      res.status(200).json(report);
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  router.get("/report/invoice", async function(req, res) {
    try {
      const report = await reportByInvoice(db, req);
      res.status(200).json(report);
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  router.get("/report/import", async function(req, res) {
    try {
      const report = await reportByImport(db, req);
      res.status(200).json(report);
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  router.get("/report/profit", async function(req, res) {
    try {
      const outcome = await reportByImport(db, req);
      const income = await reportByInvoice(db, req);
      res.status(200).json({ outcome, income });
    } catch (error) {
      res.status(500).json({ status: "error", message: error });
    }
  });

  return router;
}

module.exports = {
  createRouter
};
