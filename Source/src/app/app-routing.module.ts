import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { Shell } from '@app/shell/shell.service';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/storage' },
  Shell.childRoutes([
    // {
    //   path: 'about',
    //   loadChildren: () => import('./pages/about/about.module').then(m => m.AboutModule)
    // },
    {
      path: 'storage',
      loadChildren: () => import('./pages/storage/storage.module').then(m => m.StorageModule)
    },
    {
      path: 'books',
      loadChildren: () => import('./pages/books/books.module').then(m => m.BooksModule)
    },
    {
      path: 'import',
      loadChildren: () => import('./pages/import/import.module').then(m => m.ImportModule)
    },
    {
      path: 'cart',
      loadChildren: () => import('./pages/cart/cart.module').then(m => m.CartModule)
    },
    {
      path: 'report',
      loadChildren: () => import('./pages/report/report.module').then(m => m.ReportModule)
    },
    {
      path: 'employees',
      loadChildren: () => import('./pages/employees/employees.module').then(m => m.EmployeesModule)
    },
    {
      path: 'addEmployees',
      loadChildren: () => import('./pages/add-employee/add-employee.module').then(m => m.AddEmployeesModule)
    },
    {
      path: 'editEmployee',
      loadChildren: () => import('./pages/edit-employee/edit-employee.module').then(m => m.EditEmployeeModule)
    }
  ]),
  // Fallback when no prior route is matched
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
