import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { Credentials, CredentialsService } from './credentials.service';
import { catchError, map, tap } from 'rxjs/operators';

import { HttpService } from './../http/http.service';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';

const URL = `${environment.serverUrl}/auth`;

export interface LoginContext {
  email: string;
  password: string;
  remember?: boolean;
}

/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(private credentialsService: CredentialsService, private http: HttpService) {}

  /**
   * Authenticates the user.
   * @param context The login parameters.
   * @return The user credentials.
   */
  login(context: LoginContext): Observable<Credentials> {
    // Replace by proper authentication call
    return this.http.post(URL, context).pipe(
      catchError(error => throwError(error)),
      tap((res: Credentials) => this.credentialsService.setCredentials(res, context.remember))
    );
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.credentialsService.setCredentials();
    return of(true);
  }
}
