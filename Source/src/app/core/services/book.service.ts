import { Book } from '@app/shared/models/book.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { map } from 'rxjs/operators';

@Injectable()
export class BookService {
  url = `${environment.serverUrl}/book`;

  constructor(private http: HttpClient) {}

  getAll(params?: any): Observable<Book[]> {
    return this.http.get(this.url, { params }).pipe(map((res: any[]) => (res || []).map(i => new Book(i))));
  }

  getSingle(id: string): Observable<Book> {
    return this.http.get(`${this.url}/${id}`).pipe(map(res => new Book(res)));
  }

  post(book: Book): Promise<any> {
    const body = { ...book.flatten() };
    
    return this.http.post(this.url, body).toPromise();
  }

  update(book: Book): Promise<any> {
    const body = { ...book.flatten() };
    return this.http.put(`${this.url}/${book.id}`, body).toPromise();
  }

  increaseAmount(book: Book, amount: number): Promise<any> {
    return this.http.put(`${this.url}/${book.id}/import/${amount}`, '').toPromise();
  }
}
