import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Book } from '@app/shared/models/book.model';
import { EmployeesModel } from '@app/shared/models/employees.model';
import { Customer } from '@app/shared/models/customer.model';
import { environment } from '@env/environment';
import { HttpClient } from '@angular/common/http';

const URL = `${environment.serverUrl}/invoice`;

@Injectable()
export class CartService {
  private _cart$ = new BehaviorSubject<Book[]>([]);

  get cart$() {
    return this._cart$;
  }

  get cart() {
    return this._cart$.value;
  }

  constructor(private http: HttpClient) {}

  addBook(book: Book) {
    const cart = [...this.cart];
    const existedBook = cart.find(i => i.id === book.id);
    if (existedBook) {
      existedBook.amount += book.amount;
      return this._cart$.next(cart);
    }

    cart.push(new Book(book));
    this._cart$.next(cart);
  }

  removeBook(bookId: Book['id']) {
    const instanceIndex = this.cart.findIndex(i => i.id == bookId);
    if (instanceIndex > -1) {
      const cart = [...this.cart];
      cart.splice(instanceIndex, 1);
      this._cart$.next(cart);
    }
  }

  reset() {
    this._cart$ = new BehaviorSubject<Book[]>([]);
  }

  createInvoice(books: Book[], customer: Customer) {
    // Remove unnecessary data
    const trimmedBooks = [...books].map(i => {
      delete i.imageBase64;
      delete i.imageUrl;
      return i;
    });

    const body = {
      customer: customer,
      books: trimmedBooks
    };
    return this.http.post(URL, body).toPromise();
  }
}
