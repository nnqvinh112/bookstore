import { EmployeesModel } from '@app/shared/models/employees.model';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { delay, map } from 'rxjs/operators';
import { environment } from '@env/environment';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '@app/core/authentication/authentication.service';

@Injectable()
export class EmloyeesService {
  url = `${environment.serverUrl}/employee`;

  constructor(private http: HttpClient, private authService: AuthenticationService) {}
  getAll(): Observable<EmployeesModel[]> {
    return this.http.get(this.url).pipe(map((res: any[]) => res.map(i => new EmployeesModel(i))));
  }
  getSingle(id: string): Observable<EmployeesModel> {
    return this.http.get(`${this.url}/${id}`).pipe(map(res => new EmployeesModel(res)));
  }
  update(itemEmployee: EmployeesModel): Promise<any> {
    const body = {
      ...itemEmployee
      // createdBy: this.authService.current.id,
      // updatedBy: this.authService.current.id
    };
    console.log(body);

    return this.http.put(`${this.url}/${itemEmployee.id}`, body).toPromise();
  }
  post(itemEmployee: EmployeesModel): Promise<any> {
    const body = {
      ...itemEmployee,
    

      // createdBy: this.authService.current.id,
      // updatedBy: this.authService.current.id
    };
    console.log(body);

    return this.http.post(this.url, body).toPromise();
  }
}
