import { Observable, of } from 'rxjs';

import { Injectable } from '@angular/core';
import { BaseModel } from '@app/shared/models/base.model';
import { delay, map } from 'rxjs/operators';
import { environment } from '@env/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class GenreService {
  url = `${environment.serverUrl}/genre`;

  constructor(private http: HttpClient) {}

  getAll(): Observable<BaseModel[]> {
    return this.http.get(this.url).pipe(map((res: []) => (res || []).map(i => new BaseModel(i))));
  }
}
