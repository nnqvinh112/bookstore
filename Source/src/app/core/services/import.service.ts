import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Book } from '@app/shared/models/book.model';
import { environment } from '@env/environment';

const URL = `${environment.serverUrl}/import`;

@Injectable()
export class ImportService {
  constructor(private http: HttpClient) {}

  importSingle(book: Book, amount: number): Promise<any> {
    return this.http.put(`${URL}/book/${book.id}`, { amount }).toPromise();
  }
}
