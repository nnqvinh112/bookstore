import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

const URL = `${environment.serverUrl}/report`;

@Injectable()
export class ReportService {
  constructor(private http: HttpClient) {}

  getReportByGenre(params?: { top: number; orderBy: string }) {
    return this.http.get(
      `${URL}/genre?top=${params ? params.top : 5}&orderby=${params ? params.orderBy : 'soldValue'}`
    );
  }

  getReportByProfit(params?: { dayOffset: number }) {
    return this.http.get(`${URL}/profit`);
  }
}
