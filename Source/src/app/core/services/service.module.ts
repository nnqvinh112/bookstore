import { BookService } from './book.service';
import { NgModule } from '@angular/core';
import { EmloyeesService } from './employees.service';
import { GenreService } from './genre.service';
import { CartService } from './cart.service';
import { ImportService } from './import.service';
import { ReportService } from './report.service';

@NgModule({
  imports: [],
  exports: [],
  providers: [BookService, EmloyeesService, GenreService, CartService, ImportService, ReportService]
})
export class ServiceModule {}
