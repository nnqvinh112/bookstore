import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddEmployeeComponent } from './add-employee.component';
import { AddEmployeeRoutingModule } from './add-employee-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AddEmployeeComponent],
  imports: [CommonModule, AddEmployeeRoutingModule, ReactiveFormsModule]
})
export class AddEmployeesModule {}
