import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from '@app/shared/models/book.model';
import { BookService, GenreService } from '@app/shared';
import { BaseModel } from '@app/shared/models/base.model';
import { map } from 'rxjs/operators';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  book: Book = new Book();
  genres: BaseModel[] = [];

  constructor(
    private route: ActivatedRoute,
    private bookService: BookService,
    private genreService: GenreService,
    private router: Router
  ) {}

  ngOnInit() {
    const params = this.route.snapshot.params;
    if (!params) {
      return;
    }
    const { id } = params;
    if (id == undefined || id == null) {
      return;
    }

    forkJoin([this.loadBook(id), this.loadGenres()]).subscribe();
  }

  loadBook(id: string) {
    return this.bookService.getSingle(id).pipe(map(res => (this.book = res)));
  }

  loadGenres() {
    return this.genreService.getAll().pipe(map(res => (this.genres = res)));
  }

  onSubmit(book: Book) {
    this.bookService.update(book).then(() => {
      this.router.navigate(['/', 'storage']);
    });
  }
}
