import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BooksRoutingModule } from './books-routing.module';
import { BooksComponent } from './books.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';

@NgModule({
  declarations: [BooksComponent],
  imports: [SharedModule, CommonModule, BooksRoutingModule, FormsModule]
})
export class BooksModule {}
