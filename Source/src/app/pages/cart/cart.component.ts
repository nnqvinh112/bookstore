import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { CartService } from '@app/shared';
import { Book } from '@app/shared/models/book.model';
import { map } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { Customer } from '@app/shared/models/customer.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  books: Book[] = [];
  checkoutForm: FormGroup;
  submitted = false;
  customer: Customer = new Customer('');
  sum: number = 0;

  constructor(private cartService: CartService, private fb: FormBuilder, private toastr: ToastrService) {}

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.cartService.cart$.subscribe(res => {
      this.books = res;
      this.sumPrice();
    });
  }

  loadImg(ItemBook: Book) {
    if (ItemBook.imageBase64) {
      return 'data:image/jpg;base64,' + ItemBook.imageBase64;
    }
    return ItemBook.imageUrl;
  }

  sumPrice() {
    let sum = 0;
    for (let i of this.books) {
      sum += i.amount * i.price;
    }

    return (this.sum = sum);
  }

  removeBook(book: Book) {
    this.cartService.removeBook(book.id);
  }

  submit() {
    this.cartService
      .createInvoice(this.books, this.customer)
      .then(() => {
        this.cartService.reset();
        this.books = [];
        this.toastr.success('Checked out successfully');
      })
      .catch(error => {
        this.toastr.error('Error happened while checking out');
      });
  }
}
