import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmployeesModel } from '@app/shared/models/employees.model';
import { Router, ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { EmloyeesService } from '@app/core/services/employees.service';
@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.scss']
})
export class EditEmployeeComponent implements OnInit {
  roleSelect = ['manager', 'employee'];
  createDate = Date.now();
  reactiveForm: FormGroup;
  submitted = false;
  itemEmployee: EmployeesModel = new EmployeesModel('');

  constructor(
    private fb: FormBuilder,
    private employeesService: EmloyeesService,
    private router: Router,
    private route: ActivatedRoute
  ) {}
  ngOnInit() {
    this.createForm();
    const params = this.route.snapshot.params;
    if (!params) {
      return;
    }
    const { id } = params;
    if (id == undefined || id == null) {
      return;
    }

    forkJoin(this.loadItem(id)).subscribe(res => {
      console.log(res);
    });
  }

  loadItem(id: string) {
    return this.employeesService.getSingle(id).pipe(map(res => (this.itemEmployee = res)));
  }
  createForm() {
    this.reactiveForm = this.fb.group({
      id: [],
      name: ['', Validators.required],
      role: ['', Validators.required],
      createDate: [''],
      code: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, Validators.pattern(/^[0-9]\d{9}$/)]]
    });
  }
  get f() {
    return this.reactiveForm.controls;
  }
  submit(itemEmployee: EmployeesModel) {
    this.submitted = true;
    this.itemEmployee = this.reactiveForm.value;

    console.log(this.reactiveForm.controls);
    this.reactiveForm.markAllAsTouched();
    if (
      this.reactiveForm.value.name &&
      this.reactiveForm.value.email &&
      this.reactiveForm.value.phone &&
      this.reactiveForm.value.role
    ) {
     
      this.employeesService.update(this.itemEmployee).then(() => {
        this.router.navigate(['/employees']);
      });
    }
  }
}
