import { Component, OnInit } from '@angular/core';
import { EmloyeesService } from '@app/core/services/employees.service';
import { EmployeesModel } from '@app/shared/models/employees.model';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  constructor(private employeesService: EmloyeesService, private route: ActivatedRoute) {}
  data: EmployeesModel[] = [];
  fillterData: EmployeesModel[] = [];
  itemEmployee: EmployeesModel = new EmployeesModel('');
  tableOffset: any;
  ngOnInit() {
    this.tableOffset = 0;
    this.loadData();
  }
  changed(event: any): void {
    this.fillterData = [...this.fillterData];
  }
  fillterEmployee(event: any) {
    let val = event.target.value.toString();
    let newArr = [];
    if (val == '') {
      return (this.fillterData = this.data);
    }
    for (let i of this.data) {
      if (i.name.toLowerCase().includes(val.toLowerCase()) || i.email.toLowerCase().includes(val.toLowerCase())) {
        newArr.push(i);
      }
    }
    this.fillterData = newArr;
  }
  get rowsEmployess() {
    return this.fillterData;
  }
  loadData() {
    this.employeesService.getAll().subscribe(res => {
      this.data = res;
      this.fillterData = this.data;
      console.log(this.data);
    });
  }
}
