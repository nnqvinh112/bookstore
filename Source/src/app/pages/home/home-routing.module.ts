import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home.component';
import { NgModule } from '@angular/core';
import { Shell } from '@app/shell/shell.service';
import { extract } from '@app/core';

const routes: Routes = [{ path: 'home', component: HomeComponent, data: { title: extract('Home') } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class HomeRoutingModule {}
