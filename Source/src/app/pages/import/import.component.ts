import { Component, OnInit } from '@angular/core';
import { BaseModel } from '@app/shared/models/base.model';
import { GenreService, BookService } from '@app/shared';
import { map } from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import { Book } from '@app/shared/models/book.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.css']
})
export class ImportComponent implements OnInit {
  genres: BaseModel[] = [];

  constructor(private genreService: GenreService, private bookService: BookService, private router: Router) {}

  ngOnInit() {
    forkJoin([this.loadGenres()]).subscribe();
  }

  loadGenres() {
    return this.genreService.getAll().pipe(map(res => (this.genres = res)));
  }

  onSubmit(book: Book) {
    this.bookService.post(book).then(() => {
      this.router.navigate(['/', 'storage']);
    });
  }
}
