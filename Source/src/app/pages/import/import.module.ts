import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImportRoutingModule } from './import-routing.module';
import { ImportComponent } from './import.component';
import { SharedModule } from '@app/shared';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ImportComponent],
  imports: [SharedModule, CommonModule, ImportRoutingModule, FormsModule]
})
export class ImportModule {}
