import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label, Color } from 'ng2-charts';

@Component({
  selector: 'app-report-by-employee',
  templateUrl: './report-by-employee.component.html',
  styleUrls: ['./report-by-employee.component.scss']
})
export class ReportByEmployeeComponent implements OnInit {
  dataset: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56], label: 'Imported' },
    { data: [28, 48, 40, 19, 86], label: 'Sold' }
  ];
  labels: Label[] = ['Nam Pham', 'Nhan Le', 'Tai Nguyen', 'Vinh Nguyen', 'Hoang Vuong'];
  options: ChartOptions = {
    responsive: true,
    plugins: { datalabels: { anchor: 'end', align: 'end' } }
  };
  colors: Color[] = [
    {
      // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    {
      // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];

  constructor() {}

  ngOnInit() {}
}
