import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportByGenreComponent } from './report-by-genre.component';

describe('ReportByGenreComponent', () => {
  let component: ReportByGenreComponent;
  let fixture: ComponentFixture<ReportByGenreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReportByGenreComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportByGenreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
