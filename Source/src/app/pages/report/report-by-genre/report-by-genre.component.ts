import { Component, OnInit } from '@angular/core';
import { RadialChartOptions, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { ReportService } from '@app/core/services/report.service';

@Component({
  selector: 'app-report-by-genre',
  templateUrl: './report-by-genre.component.html',
  styleUrls: ['./report-by-genre.component.scss']
})
export class ReportByGenreComponent implements OnInit {
  options: RadialChartOptions = { responsive: true, scale: { ticks: { display: false } } };
  labels: Label[] = [];
  dataset: ChartDataSets[] = [{ data: [] }];

  constructor(private reportService: ReportService) {}

  ngOnInit() {
    this.reportService.getReportByGenre().subscribe((res: any[]) => {
      this.labels = res.map(i => i.name);
      this.dataset = [{ data: res.map(i => i.soldValue) }];
    });
  }
}
