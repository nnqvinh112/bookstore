import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportByTimeComponent } from './report-by-time.component';

describe('ReportByTimeComponent', () => {
  let component: ReportByTimeComponent;
  let fixture: ComponentFixture<ReportByTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReportByTimeComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportByTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
