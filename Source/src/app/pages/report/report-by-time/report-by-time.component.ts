import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { ReportService } from '@app/core/services/report.service';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-report-by-time',
  templateUrl: './report-by-time.component.html',
  styleUrls: ['./report-by-time.component.scss']
})
export class ReportByTimeComponent implements OnInit {
  decimalPipe = new DecimalPipe('vi');
  public pieChartOptions: ChartOptions = {
    responsive: true,
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          return this.decimalPipe.transform(value);
        }
      }
    }
  };
  public pieChartLabels: Label[] = [['Income'], ['Outcome']];
  public pieChartData: number[] = [0, 0];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [pluginDataLabels];
  public pieChartColors = [
    {
      backgroundColor: ['rgba(255,0,0,0.3)', 'rgba(0,255,0,0.3)', 'rgba(0,0,255,0.3)']
    }
  ];

  constructor(private reportService: ReportService) {}

  ngOnInit() {
    this.reportService.getReportByProfit().subscribe((res: any) => {
      this.pieChartData = [res.income.totalValue, res.outcome.totalValue];
    });
  }
}
