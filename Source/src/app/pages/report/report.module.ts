import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts';
import { ReportRoutingModule } from './report-routing.module';
import { ReportComponent } from './report.component';
import { FormsModule } from '@angular/forms';
import { ReportByTimeComponent } from './report-by-time/report-by-time.component';
import { ReportByEmployeeComponent } from './report-by-employee/report-by-employee.component';
import { ReportByGenreComponent } from './report-by-genre/report-by-genre.component';

@NgModule({
  declarations: [ReportComponent, ReportByTimeComponent, ReportByEmployeeComponent, ReportByGenreComponent],
  imports: [CommonModule, ReportRoutingModule, ChartsModule, FormsModule]
})
export class ReportModule {}
