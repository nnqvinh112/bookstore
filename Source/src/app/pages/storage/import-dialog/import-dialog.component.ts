import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Book } from '@app/shared/models/book.model';
import { BsModalRef } from 'ngx-bootstrap';
import { BookService } from '@app/shared';
import { ImportService } from '@app/core/services/import.service';

@Component({
  selector: 'app-import-dialog',
  templateUrl: './import-dialog.component.html',
  styleUrls: ['./import-dialog.component.scss']
})
export class ImportDialogComponent implements OnInit {
  @Input() book: Book;

  @Output() result = new EventEmitter();

  amount = 1;

  constructor(public modalRef: BsModalRef, private bookService: BookService, private importService: ImportService) {}

  ngOnInit() {}

  update() {
    this.importService.importSingle(this.book, this.amount).then(() => {
      this.result.next(true);
      this.modalRef.hide();
    });
  }
}
