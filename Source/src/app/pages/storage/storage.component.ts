import { BookService, CartService } from '@app/core/services';
import { Component, OnInit } from '@angular/core';

import { Book } from '@app/shared/models/book.model';
import { BsModalService } from 'ngx-bootstrap';
import { ImportDialogComponent } from './import-dialog/import-dialog.component';
import { map } from 'rxjs/operators';

interface IStorageItem extends Book {
  amountInCart: number;
}

@Component({
  selector: 'app-storage',
  templateUrl: './storage.component.html',
  styleUrls: ['./storage.component.scss']
})
export class StorageComponent implements OnInit {
  rows: IStorageItem[] = [];
  searchValue = '';

  constructor(
    private bookService: BookService,
    private modalService: BsModalService,
    private cartService: CartService
  ) {}

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.loadRows().toPromise();
  }

  loadRows() {
    return this.bookService
      .getAll({
        id: this.searchValue,
        title: this.searchValue,
        author: this.searchValue,
        publisher: this.searchValue
      })
      .pipe(
        map(res => {
          this.rows = [
            ...res.map(i => {
              const item = i as IStorageItem;
              const bookInCart = this.cartService.cart.find(x => x.id === i.id);
              item.amountInCart = bookInCart ? bookInCart.amount : 0;
              return item;
            })
          ];
        })
      );
  }

  import(book: Book) {
    this.modalService
      .show(ImportDialogComponent, {
        initialState: { book }
      })
      .content.result.subscribe((success: boolean) => {
        if (success) {
          this.loadData();
        }
      });
  }

  addToCart(book: IStorageItem) {
    this.cartService.addBook(new Book({ ...book, amount: 1 }));
    book.amountInCart += 1;
  }
}
