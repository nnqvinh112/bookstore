import { RouterModule, Routes } from '@angular/router';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { StorageComponent } from './storage.component';
import { ImportDialogComponent } from './import-dialog/import-dialog.component';
import { ModalModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';

const routes: Routes = [{ path: '', component: StorageComponent }];

@NgModule({
  declarations: [StorageComponent, ImportDialogComponent],
  entryComponents: [ImportDialogComponent],
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    NgxDatatableModule,
    ModalModule.forRoot()
  ]
})
export class StorageModule {}
