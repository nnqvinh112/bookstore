export function blob2Base64(blobParts: any[]) {
  const blobArr = new Uint8Array(blobParts);
  return btoa(
    blobArr.reduce((data: any, byte: any) => {
      return data + String.fromCharCode(byte);
    }, '')
  );
}
