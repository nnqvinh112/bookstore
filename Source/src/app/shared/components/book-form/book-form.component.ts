import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Book } from '@app/shared/models/book.model';
import { BaseModel } from '@app/shared/models/base.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss']
})
export class BookFormComponent implements OnInit {
  @Input() book: Book = new Book();
  @Input() genres: BaseModel[] = [];
  @Input() actionLabel: string = 'Save';
  @Input() showCode = false;

  @Output() actionClick = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  onSubmit(form: NgForm) {
    form.form.markAllAsTouched();
    if (form.valid) {
      const genre = (this.genres || []).find(i => i.id.toString() === form.value.genre.toString());
      this.actionClick.next(new Book({ ...form.value, genre }));
    }
  }
}
