import { LoaderComponent } from './loader/loader.component';
import { NgModule } from '@angular/core';
import { BookFormComponent } from './book-form/book-form.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PipeModule } from '../pipes/pipe.module';

@NgModule({
  imports: [CommonModule, FormsModule, PipeModule],
  exports: [LoaderComponent, BookFormComponent],
  declarations: [LoaderComponent, BookFormComponent],
  providers: []
})
export class ComponentModule {}
