import { BaseModel } from './base.model';

export class Account extends BaseModel {
  username: string;
  email: string;

  constructor(data?: any) {
    super(data);
    if (data) {
      this.username = data.username;
      this.email = data.email;
    }
  }
}
