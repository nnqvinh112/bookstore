export class BaseModel {
  name: string;
  id: string;

  constructor(item?: any) {
    if (item) {
      this.name = item.name;
      this.id = item.id;
    }
  }
}
