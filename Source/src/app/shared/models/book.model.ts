import { BaseModel } from './base.model';
import { EmployeesModel } from './employees.model';
import { Genre } from './genre.model';
import { blob2Base64 } from '../common/converter';

export class Book extends BaseModel {
  description: string;
  author: string;
  publisher: string;
  price: number;
  amount: number;
  genre: Genre = new Genre();
  createdBy: EmployeesModel;
  imageUrl: any;
  imageBase64: any;

  get isLow() {
    return this.amount < 5;
  }

  constructor(item?: any) {
    super(item);

    if (item) {
      this.name = item.name || item.title;
      this.description = item.description;
      this.author = item.author;
      this.publisher = item.publisher;
      this.price = item.price;
      this.amount = item.amount;
      this.genre = new Genre(item.genre);
      this.createdBy = new EmployeesModel(item.createdBy);
      this.imageUrl = 'assets/No-Image-Available.jpg';
      this.imageBase64 = item.imageBase64;
      if (item.image) {
        try {
          this.imageBase64 = blob2Base64(item.image.data);
        } catch (error) {
          console.error(error);
        }
      }
    }
  }

  flatten() {
    return {
      ...this,
      title: this.name,
      genre: this.genre.id,
      createdBy: this.createdBy.id
    };
  }
}
