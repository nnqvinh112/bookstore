import { EmployeesModel } from './employees.model';

export class Customer{
    name: String;
    id: number;
    phone: String;
    email: String;
    createdDate: Date;
    createdBy: EmployeesModel
    constructor(item: any){
        if(item){
            this.name = item.name;
            this.id = item.id;
            this.phone = item.phone;
            this.email = item.email;
            this.createdDate = item.createdDate;
            this.createdBy = new EmployeesModel(item.createdBy);
        }
    }
}