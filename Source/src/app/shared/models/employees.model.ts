export class EmployeesModel {
  name: String;
  id: number;
  phone: String;
  email: String;
  createdDate: Date;
  role: String;

  constructor(obj: any) {
    if (obj) {
      this.name = obj.name;
      this.id = obj.idEmployees || obj.id;
      this.phone = obj.phone;
      this.email = obj.email;
      this.createdDate = obj.createdDate;
      this.role = obj.role;
    }
  }
  flatten() {
    return {
      ...this,
      createdBy : this.id
    };
  }
}
