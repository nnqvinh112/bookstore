import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'base64Image'
})
export class Base64ImagePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}

  transform(value: any, ...args: any[]): any {
    if (!value) {
      return 'assets/No-Image-Available.jpg';
    }
    return this.sanitizer.bypassSecurityTrustUrl('data:image/jpg;base64, ' + value);
  }
}
