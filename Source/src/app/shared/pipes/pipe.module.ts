import { NgModule } from '@angular/core';
import { VndPipe } from './vnd.pipe';
import { Base64ImagePipe } from './base64-image.pipe';

@NgModule({
  imports: [],
  exports: [VndPipe, Base64ImagePipe],
  declarations: [VndPipe, Base64ImagePipe],
  providers: []
})
export class PipeModule {}
