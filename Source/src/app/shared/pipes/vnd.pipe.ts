import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Pipe({
  name: 'vnd'
})
export class VndPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    return new CurrencyPipe('vi').transform(value, 'VND', 'symbol');
  }
}
