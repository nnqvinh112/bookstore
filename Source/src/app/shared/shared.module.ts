import { CommonModule } from '@angular/common';
import { ComponentModule } from './components/component.module';
import { NgModule } from '@angular/core';
import { PipeModule } from './pipes/pipe.module';

@NgModule({
  imports: [CommonModule, ComponentModule, PipeModule],
  declarations: [],
  exports: [ComponentModule, PipeModule]
})
export class SharedModule {}
